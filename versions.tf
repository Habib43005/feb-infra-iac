terraform {
  backend "s3" {
    bucket = "feb-tf-eks-terraform-state-bucket"
    key    = "eks-cluster/terraform.tfstate"
    region = "us-west-2"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.39.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.10.0"
    }
    helm = {
      source = "hashicorp/helm"
      version = "2.7.1"
    }
  }
}
